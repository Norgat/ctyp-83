(ns examples.core
  (:require [clojure.core.typed :refer [ann check-ns cf]]))


(ann foo (Fn [String -> nil]))
(ann doom Number)
(ann temp (Fn [-> nil]))
(ann tmp (Fn [Number Number -> Number]))


(defn ^{:export 1} foo
  "I don't do a whole lot."
  [x]
  (println x))


(def doom 10)


(defn temp []
  (println "Hello"))


(defn tmp [a b]
  (+ a b))
