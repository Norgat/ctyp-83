(defproject examples "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :plugins [[lein-typed "0.3.1"]]

  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/core.typed "0.2.34"]]

  :core.typed {
               :check [examples.pathological]}
  )
